Download
========

You can obtain a copy of LIBPFASST through the `LIBPFASST project
page`_ on `bitbucket.org`_.  Complilation instructions can be found in
the `README`_ file included with LIBPFASST.

.. _`LIBPFASST project page`: https://bitbucket.org/memmett/libpfasst
.. _`bitbucket.org`: https://bitbucket.org
.. _`README`: https://bitbucket.org/memmett/libpfasst/raw/master/README
