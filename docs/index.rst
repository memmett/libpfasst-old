PFASST
======

The **Parallel Full Approximation Scheme in Space and Time** (PFASST)
algorithm is a method for parallelizing ODEs and PDEs in time.  The
maths behind the PFASST algorithm are breifly described on the
:doc:`maths` page.

This work was supported by the Director, DOE Office of Science, Office
of Advanced Scientific Computing Research, Office of Mathematics,
Information, and Computational Sciences, Applied Mathematical Sciences
Program, under contract DE-SC0004011.  This work is currently authored
by `Michael L. Minion`_ and `Matthew Emmett`_.  Contributions are
welcome -- please contact `Matthew Emmett`_.


LIBPFASST
=========

LIBPFASST is a Fortran implementation of the PFASST algorithm.

**Main parts of the documentation**

* :doc:`Download <download>` - download and installation instructions.
* :doc:`Tutorial <tutorial>` - getting started and basic usage.
* :doc:`Overview <overview>` - design and interface overview.
* :doc:`Reference <reference>` - information about the internals of LIBPFASST.
* :doc:`Communicators <communicators>` - information about the various communicators available.


Contributing
------------

LIBPFASST is released under the GNU GPL, please see the LICENSE file
in the source code repository for more information.

If you would like to contribute to the development of LIBPFASST,
please, dive right in by visiting the `LIBPFASST project page`_.


.. _`Michael L. Minion`: http://amath.unc.edu/Minion/Minion
.. _`Matthew Emmett`: http://emmett.ca/matthew/
.. _`LIBPFASST project page`: https://bitbucket.org/memmett/libpfasst

.. toctree::
   :hidden:

   self
   maths
   download
   tutorial
   overview
   reference
   communicators
   parameters
